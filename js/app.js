
const API_KEY = `4baba9e5acc1fbfdc18b78f65edd7c35`;
const LIST_POPULATE = `http://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}`;
const MOVIE_DETAIL = (id) => { return `http://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}` };
const MOVIE_DETAIL_KEYWORD = (id) => { return `http://api.themoviedb.org/3/movie/${id}/keywords?api_key=${API_KEY}` };
const MOVIE_DETAIL_SIMILAR = (id) => { return `http://api.themoviedb.org/3/movie/${id}/credits?api_key=${API_KEY}` };
const POST_IMAGE = (path) => { return `https://image.tmdb.org/t/p/w300_and_h450_bestv2${path}` };
const POST_SILIMAR = (path) => { return path === null ? 'http://image.phimmoi.net/profile/default/male/thumb.jpg' : `https://image.tmdb.org/t/p/w138_and_h175_face${path}` };
const MOVIE_ACTOR = (id) => { return `http://api.themoviedb.org/3/person/${id}?api_key=${API_KEY}` }
const MOVIE_ACTOR_CREDIT = (id) => { return `https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${API_KEY}` };
const POST_IMAGES_ACTOR = (path) => { return `https://image.tmdb.org/t/p/w600_and_h900_bestv2${path}` };
const POST_IMAGES_ACTOR_CREDIT = (path) => { return `https://image.tmdb.org/t/p/w150_and_h225_bestv2${path}` };
const MOVIE_DETAIL_REVIEW = (id) => { return `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${API_KEY}` };
const MOVIE_SEARCH = (id) => { return `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}` }

function generalElement(title, time, description, image, id) {
    let html =
        `
        <div class="col-md-6 item">
            <div class="row">
                <div class="col-md-5 no-padding">
                    <div class="item-image">
                        <a href="./detail.html?id=${id}">
                            <img width="100%"
                                src="https://image.tmdb.org/t/p/w500/${image}">
                        </a>
                        <div class="item-image-footer">
                            <h5 style="font-weight:bold;">the movie</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 item-content no-padding">
                    <div class="padding">
                        <div>
                            <a href="./detail.html?id=${id}" class="title capitalize">${title}</a>
                            <p class="capitalize">${time}</p>
                        </div>
                        <div>
                            <p class="description">${description}</p>
                        </div>
                    </div>
                    <div class="item-content-footer ">
                        <a class="capitalize ">more info </a>
                    </div>
                </div>
            </div>
        </div>
    `;
    return html;
}



function GetListPopulate() {

    $.ajax({
        url: 'http://api.themoviedb.org/3/movie/popular?api_key=4baba9e5acc1fbfdc18b78f65edd7c35',
        type: 'GET',
        dataType: 'json'
    }).done(function (data) {
        $.each(data.results, function (key, item) {
            $('#movie').append(generalElement(item.original_title, item.release_date, item.overview, item.poster_path, item.id))
        })
    })
}



function generalElementKeyWord(keywords) {
    let html = `
    <div class="col-md-6 item-keyword">
        <a class="btn btn-primary" href="#" role="button">${keywords}</a>
    </div>
    `;
    return html;
}

function GetInformation() {
    const id = getParamsUrl("id");
    if (id !== false) {
        GetData(MOVIE_DETAIL(id), function (data) {
            $('#image').attr("src", POST_IMAGE(data.poster_path))
            $('#title').text(data.original_title)
            $('#overview').text(data.overview);
            $('.backgroundfirst').css('background-image', `url(https://image.tmdb.org/t/p/w1400_and_h450_face${data.backdrop_path})`)
        })
        GetData(MOVIE_DETAIL_KEYWORD(id), function (data) {
            $.each(data.keywords, function (key, item) {
                $('#keywords').append(generalElementKeyWord(item.name))
            })
        })
        GetData(MOVIE_DETAIL_SIMILAR(id), function (data) {
            var length_data = data.cast.length;
            if (length_data < 5) {
                for (let i = 0; i < length_data; i++) {
                    $('#similar').append(getElementImages(POST_SILIMAR(data.cast[i].profile_path), data.cast[i].name, data.cast[i].character, data.cast[i].id))
                }
            }
            else {
                for (let i = 0; i < 5; i++) {
                    $('#similar').append(getElementImages(POST_SILIMAR(data.cast[i].profile_path), data.cast[i].name, data.cast[i].character, data.cast[i].id))
                }
            }
        })
        GetData(MOVIE_DETAIL_REVIEW(id), function (data) {

            var x = data.results;
            $('#movie_reviews').text(x.content);
            $('#movie_reviews').text(x.author);
        })
    }
}



function getActor() {
    const id = getParamsUrl("id");
    GetData(MOVIE_ACTOR(id), function (data) {
        $('#actor_image').attr("src", POST_IMAGES_ACTOR(data.profile_path))
        $('#actor_birtgday').text(data.birthday)
        $('#actor_place_of_birth').text(data.place_of_birth)
        $('#actor_name').text(data.name)
        $('#actor_biography').text(data.biography)
    })

    GetData(MOVIE_ACTOR_CREDIT(id), function (data) {
        var length_data = data.cast.length;
        if (length_data < 8) {
            for (i = 0; i < length_data; i++) {
                $('#actor_MovieImages').append(getActor_Images(POST_IMAGES_ACTOR_CREDIT(data.cast[i].poster_path), data.cast[i].title))
            }
        } else {
            for (i = 0; i < 8; i++) {
                $('#actor_MovieImages').append(getActor_Images(POST_IMAGES_ACTOR_CREDIT(data.cast[i].poster_path), data.cast[i].title))
            }
        }
    })


}


function getParamsUrl(params, decode = true) {
    let urlLocation = window.location.search;
    if (urlLocation == '') {
        window.location.href = "./404.html"
    } else {
        let arr = urlLocation.split("?")[1].split("&");
        for (let i = 0; i < arr.length; i++) {
            let paramArr = arr[i].split("=");
            if (paramArr[0] === params) {
                return (decode) ? decodeURIComponent(paramArr[1]) : paramArr[1]
            }
            return false;
        }
    }

}

function getActor_Images(image, title) {
    let html = `<div class="col-md-3 item-Actor_MovieImages">
<a><img
        src="${image}"></a>
<a>
    <p class="align">${title}</p>
</a>
</div>`;
    return html;
}


function getElementImages(image, name, character, id) {
    let html = `
    <div class="col-md-2dot4">
        <a href="./actor.html?id=${id}" >
            <img src="${image}" width="100%">
        </a>
        <div>
            <a>${name}</a>
            <p>${character}</p>
        </div>
    </div>
    `;
    return html;
}




function GetData(API, callback, type = 'GET', dataType = 'json') {
    $.ajax({
        url: API,
        type: type,
        dataType: dataType
    }).done((data) => callback(data))
}
